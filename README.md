# urlmeta
*Arsenio Santos <arsenio@gmail.com>*

A small web service, written in Go, that responds to GET requests (where the caller passes in a URL) with some information about that URL.

## Dependencies
To run urlmeta, you will need to have Go installed. You can find the binary [here](https://golang.org/dl/)

## Build And Run
To create the local binary, just run `make`.

Once you have your binary, you can operate it with the following:

    ./urlmeta <port-number>

## Test Coverage
To run the unit tests for urlmeta, just run:

    make test
