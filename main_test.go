package main

import (
    "encoding/json"
    "fmt"
    "net/http/httptest"
    "testing"
)

//func TestMain(t *testing.T) {
//    val := GetName()
//    if val != "urlmeta" {
//        t.Fail()
//    }
//}

func SendResponseHandler(t *testing.T, params ...string) {
    w := httptest.NewRecorder()

    status := "ok"
    description := ""
    reporter := "urlmeta"

    if len(params) >= 1 {
        status = params[0]
    }

    if len(params) >= 2 {
        description = params[1]
    }

    if len(params) >= 3 {
        reporter = params[2]
    }

    SendResponse(w, status, description, reporter)

    var msg Message
    err := json.Unmarshal(w.Body.Bytes(), &msg)
    if err != nil {
        fmt.Println("error:", err)
    }

    if msg.Status != status {
        t.Fail()
    }

    if msg.Description != description {
        t.Fail()
    }

    if msg.Reporter != reporter {
        t.Fail()
    }
}

func TestSendResponseWithStatusOnly(t *testing.T) {
    SendResponseHandler(t, "ok")
}

func TestSendResponseWithStatusAndDescription(t *testing.T) {
    SendResponseHandler(t, "ok", "description")
}

func TestSendResponseWithFullMessage(t *testing.T) {
    SendResponseHandler(t, "ok", "description", "testharness")
}
