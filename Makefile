NAME = urlmeta

.PHONY: default rundev

default: $(NAME)
$(NAME): $(shell find . -name \*.go)
	go build

rundev:
	go run main.go

test:
	go test
