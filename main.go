// Server instance for fetching metainfo on specified URLs.
//
// @author Arsenio Santos <arsenio@gmail.com>
package main

import (
    "encoding/json"
    "net/http"
    "log"
    "os"
    "time"
)

// Determine the listener port, set up the URL handlers, and start the server.
func main() {
    domainWithPort := ":80"
    args := os.Args
    if len(args) > 1 {
        domainWithPort = ":" + args[1]
    }

    log.Printf("urlmeta server listening at %v, started at %v\n", domainWithPort, time.Now())

    http.HandleFunc("/", CatchallHandler)
    http.HandleFunc("/urlinfo/1/", FetchHandler)

    log.Fatal(http.ListenAndServe(domainWithPort, nil))
}

// Message encapsulates all the response data for any given request.
type Message struct {
    Status string
    Description string
    Reporter string
    Time time.Time
}

// This method genericizes the HTTP response message, then cascades to a JSON outputter.
func SendResponse(w http.ResponseWriter, params ...string) {
    status := "ok"
    description := ""
    reporter := "urlmeta"

    if len(params) >= 1 {
        status = params[0]

        // While a robust response message coud be helpful in many cases,
        // a simple thumbs-up-or-down would suffice for many others.
        // Here, we use the HTTP response status code as a red/green indicator.
        if status != "ok" {
            w.WriteHeader(http.StatusBadGateway)
        }
    }

    if len(params) >= 2 {
        description = params[1]
}

    if len(params) >= 3 {
        reporter = params[2]
    }

    WriteJSONResponse(w, Message{status, description, reporter, time.Now()})
}

// This method will set the content type, encode the message as JSON, and send the response.
// Also, note that we're using the standard JSON library and encoder here and only here,
// largely for ease and readability, but that there is likely a slight-to-modest
// performance gain to be had here with a simple string formatter instead.
func WriteJSONResponse(w http.ResponseWriter, msg Message) {
    w.Header().Set("Content-Type", "application/json")
    enc := json.NewEncoder(w)
    enc.Encode(&msg)
}

// This handler only exists to catch requests other that proper URL info requests.
func CatchallHandler(w http.ResponseWriter, req *http.Request) {
    SendResponse(w, "ok", "urlmeta ok")
}

// This handler will get invoked for proper URL info requests.
func FetchHandler(w http.ResponseWriter, req *http.Request) {
    // By default description, assume a miss (ie: a perfectly allowable URL)
    status := "ok"
    description := req.URL.Path[11:] + " is safe"

    // TODO: implement per-URL lookups

    SendResponse(w, status, description)
}
